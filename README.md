
# theme-Hx-LR

#### 介绍

VS Code 主题，根据HBuilderX 的默认主题绿柔 改的配色

#### 安装

将theme-Hx-LR 克隆/下载到本地 放进指定文件夹下：

指定文件夹：VS Code安装目录\Microsoft VS Code\resources\app\extensions

![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/171435_dc3081ee_8757542.png "image-20210920165907931.png")
打开VSCode 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/171458_39c44872_8757542.png "image-20210920170003394.png")
选择

![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/171508_6d54f9a8_8757542.png "image-20210920170033613.png")

重新启动VSCode即可。

#### 效果图：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/171523_d953ced9_8757542.png "image-20210920171037613.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/171534_06aff99e_8757542.png "image-20210920171114119.png")

#### 说明

由于配色太多，有些没有配置，代码里基本都加了中文说明，可以自行修改

下面是一位大佬的博客:

[VSCode自定义配色方案 - garvenc - 博客园 (cnblogs.com)](https://www.cnblogs.com/garvenc/p/vscode_customize_color_theme.html)

后续发现问题会再修改上传（有使用的也可以提提建议）

